'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    less  = require('gulp-less'),
    LessPluginCleanCSS = require("less-plugin-clean-css"),
    cleancss = new LessPluginCleanCSS({advanced: true}),

    LessPluginAutoPrefix = require('less-plugin-autoprefix'),
    autoprefix= new LessPluginAutoPrefix({browsers: ["last 2 versions"]}),

    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),

    plumber = require('gulp-plumber'),
    pug = require('gulp-pug'),
    data = require('gulp-data'),
    fs = require('fs'),
    path_json = require('path'),
    merge = require('gulp-merge-json'),
    runSequence = require('run-sequence'),

    zip = require('gulp-zip'),
    prettify = require('gulp-jsbeautifier'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;



var path = {
    build: {
        pug:    'src/template/',
        temp:   'temp/',
        html:   'build/',
        js:     'build/js/',
        css:    'build/css/',
        img:    'build/img',
        fonts:  'build/fonts',
        zip:    'dist'
    },
    src: {
        data:   'data/**/*.json',
        temp:   './temp/data.json',
        pug:    'src/template/pug/**/*.pug',
        html:   'src/*.html',
        js:     'src/js/main.js',
        style:  'src/style/main.less',
        css:  'src/style/**/*.css',
        img:    'src/img/**/*.*',
        fonts:  'src/fonts/**/*.*'
    },
    watch: {
        pug:    'src/template/pug/**/*.pug',
        temp:   'temp/data.json',
        data:   'data/**/*.json',
        html:   'src/**/*.html',
        js:     'src/js/**/*.js',
        style:  'src/style/**/*.less',
        css:  'src/style/**/*.css',
        img:    'src/img/**/*.*',
        fonts:  'src/fonts/**/*.*'
    },
    clean: './build'

};

function requireUncached( $module ) {
    delete require.cache[require.resolve( $module )];
    return require( $module );
}


var config = {
    server: {
        baseDir:    "./build"
    },
    tunnel: false,
    host:   'localhost',
    port:   9000,
    logPrefix:  'Frontend_DevServer'
};


gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});


gulp.task('js:build',function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream:true}));
});

gulp.task('js:build-prod',function () {
    gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream:true}));
});



gulp.task('style:build', function () {
    gulp.src(path.src.style)

        .pipe(less({
            // plugins: [autoprefix, cleancss]
        }))
        .pipe(concat('main.css'))
        .pipe(prettify({
            debug: true
        }))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('style:build-prod', function () {
    gulp.src(path.src.style)
        .pipe(less({
            plugins: [autoprefix, cleancss]
        }))
        .pipe(concat('main.css'))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});


gulp.task('css:build', function () {
    gulp.src(path.src.css)
        .pipe(gulp.dest(path.build.css))
});


gulp.task('image:build', function () {
   gulp.src(path.src.img)
       .pipe(imagemin({
           progressive: true,
           svgoPlugins: [{removeViewBox: false}],
           use: [pngquant()],
           interlaced: true
       }))
       .pipe(gulp.dest(path.build.img))
       .pipe(reload({stream: true}));
});


gulp.task('fonts:build', function () {
   gulp.src(path.src.fonts)
       .pipe(gulp.dest(path.build.fonts))
});


gulp.task('pug:data', function () {
    gulp.src(path.src.data)
        .pipe(plumber())
        .pipe(merge({
            fileName: 'data.json',
            edit: function (json, file) {
                var filename = path_json.basename(file.path),
                    primaryKey = filename.replace(path_json.extname(filename), '');

                var data = {};
                data[primaryKey.toUpperCase()] = json;

                return data;
            }
        }))
        .pipe(gulp.dest(path.build.temp));
});

gulp.task('pug:build', function () {
   gulp.src(path.src.pug)
       .pipe(plumber())
       .pipe(data(function(file) {
           return requireUncached(path.src.temp);
       }))
       .pipe(pug({
           pretty: true,
           basedir: './'
       }))
       .pipe(gulp.dest(path.build.pug))
       .pipe(reload({stream: true}));
});

gulp.task('pug', function(callback){
    runSequence('pug:data',
                'pug:build',
                 callback);
});


gulp.task('zip', function () {
    gulp.src('build/**/*')
        .pipe(zip('archive.zip'))
        .pipe(gulp.dest('dist'))
});



gulp.task('build',[
    'pug',
    'js:build',
    'style:build',
    'css:build',
    'html:build',
    'fonts:build',
    'image:build'
]);

gulp.task('build-prod',[
    'pug',
    'js:build-prod',
    'style:build-prod',
    'css:build',
    'html:build',
    'fonts:build',
    'image:build'
]);




gulp.task('watch', function () {
    watch([path.watch.data], function(event, cb) {
        gulp.start('pug');
    });
    watch([path.watch.pug], function(event, cb) {
        gulp.start('pug');
    });

    watch([path.watch.html], function (event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.css], function(event, cb) {
        gulp.start('css:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });

});


gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);

